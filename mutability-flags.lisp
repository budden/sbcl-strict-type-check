; -*- system: typep-never-changes-p ; -*-
(in-package :SB-KERNEL) 

;;; Mutability
;; this object is freezed and can't be modified
;; (defconstant +mutability-flag-freezed+ 1)
;; this object is mutable and can't be freezed - unused for now
;; (defconstant +mutability-flag-unfreezable+ 2)
;; The default mutability value is 0: object is mutable, but can be freezed.

;; The non-default value for mutability flag is recorded in a weak hash table
(defvar *mutability-table* (make-hash-table :test 'eq :weakness :key))

(defvar *default-mutability-value* 1)

(defun freeze-object (o)
  "Makes sure that object is freezed. Returns t if success, nil otherwise"
  ;; FIXME Race condition here!
  (case (gethash o *mutability-table*)
    ((nil 1) (setf (gethash o *mutability-table*) 0))
    (0
     ; do nothing
     ))
  ; so far, we always succeed
  t)

(declaim (inline object-mutability-value))
(defun object-mutability-value (o)
  "0 (false) = immutable, 1 (true) = mutable. Other potentially meaningful values are:
  'can't be freezed', 'owned-by-this-thread', 'owned if monitor object is locked', 'immutable graph of objects through pointers' and so on"
  (typecase o
    (number 0)
    (character 0)
    (vector (the (integer 0 1) (or (gethash o *mutability-table*) 1)))
    (t 1)))

(defmacro ensuring-mutability (o &body body)
  (with-unique-names (%o)
    `(let ((,%o ,o))
       (typecase ,%o
         ((or (not vector) (mutable 0)) ; check mutability for vectors only
          ,@body ; is it ok to substitute body twice?
          )
         (t
          `(let ((*default-mutability-value* 0)) ; for objects consed while handling error
             (error 'simple-type-error "Object must be mutable: ~S" ,%o)))))))

(defun imm-test-fn-1 ()
  (let ((a (cl-user::copy-seq "abcd")))
    (freeze-object a)
    (setf
     ;(elt a 2) ; also works fine
     (aref a 2)
     #\i)
    a))

(defun imm-test-fn-2 ()
  (let ((a (vector 1 2 3)))
    (freeze-object a)
    (setf
     ;(elt a 2) ; also works fine
     (svref a 2)
     #\i)
    a))

(assert (typep (nth-value 1 (ignore-errors (imm-test-fn-1))) 'error))
(assert (typep (nth-value 1 (ignore-errors (imm-test-fn-2))) 'error))


