About
=====

### Case 1
```
(defun BAR (X)
  (declare (type NUMBER X))
  (flet ((FOO (Y) 
           (declare (type INTEGER Y))
           (+ Y 1)))
    (FOO X)))
```
BAR function is not type safe (number is casted to integer), but is compiled without warnings 
by SBCL.

Consequences are: 

- normally, run-time type check is inserted into the emitted code
- type check slows down the execution
- it is harder to reason about types in BAR 
- if code optimized for speed, type check is omitted and behavior is unspecified for `(bar 0.5)` 

Goal of the project is to issue a patch for SBCL compiler such that it emits warning under some compiler policy settings. 

### Case 2
```
(defparameter *g* (cons 1 2))
(defun g () (setf (car *g*) 'a))

(defun f (x)
  (when (typep x '(cons fixnum))
    (g)
    (if (typep x '(cons fixnum))
      "Still cons fixnum!"
      "Not cons fixnum")))
(f *g*)
```
Type inference is too smart here. There is SB-C::TYPE-NEEDS-CONSERVATION-P, and the situation where membership of x in (cons fixnum) type could be altered by (g) is discussed around its definition, but this example works incorrectly. 

How to use
==========
Currently nothing works :) 

- compile and load ir1opt-patch.lisp then compile and load test.lisp
- optimizer policy to enable extra checks is (safety 2)
