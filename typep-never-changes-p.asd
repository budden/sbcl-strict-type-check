
(defsystem :TYPEP-NEVER-CHANGES-P
  :author "Budden73 <budden73@mail.ru>"
  :license "Public Domain"
  :description "Part of typep-never-changes-p project under development (needs patched sbcl)"
  :depends-on (:budden-tools)
  :serial t
  :in-order-to ((test-op (test-op :TYPEP-NEVER-CHANGES-P-TESTS)))
  :components ((:file "typep-never-changes-package" :description "intern some symbols")
               (:file "late-type-patch-for-typep-never-changes")
               (:file "ir1-opt-patch-for-typep-never-changes")
               (:file "mutability-flags")
               ))




