; -*- system: typep-never-changes-p-tests ; -*-
(in-package :SB-KERNEL) ;; Sorry for that, that's horrible :) 

(defparameter *global-mutable-cons* (cons 1 nil))

(defun set-car-of-global-mutable-cons (new-value)
  (setf (car *global-mutable-cons*) new-value))

(defun cons-integer-type-derivation-fixed-test-fn (x)
  (set-car-of-global-mutable-cons 1)
  (typecase x
    ((cons integer)
     (set-car-of-global-mutable-cons "abc")
     (typecase x
       ((cons string)
        "Success")
       ((cons integer)
        "(cons integer) was derived despite mutability")
       (t
        "wtf1")))
    (t
     "wtf2")))

(defun cons-integer-type-derivation-fixed-test-fn-2 (x)
  (set-car-of-global-mutable-cons 1)
  (cons
   (the (cons integer) x)
   (progn
     (set-car-of-global-mutable-cons "abc")
     (the (cons string) x))))

(defun dispatch-by-cons-subtype (c)
  (typecase c
    ((cons symbol)
     '(cons symbol))
    ((cons integer)
     '(cons integer))
    (t
     '(cons t))))

(with-test (:name :avoid-incorrect-subtype-propagation-for-cons-integer)
           (let ((res1 (cons-integer-type-derivation-fixed-test-fn *global-mutable-cons*))
                 )
             (assert (string= "Success" res1)))
           (assert (equalp 
                    (cons-integer-type-derivation-fixed-test-fn-2 *global-mutable-cons*)
                    '(("abc") "abc")))
           (assert (equalp
                    '(cons symbol) (dispatch-by-cons-subtype '(a))))
           (assert (equalp
                    '(cons integer) (dispatch-by-cons-subtype '(1))))
           (assert (equalp
                    '(cons t) (dispatch-by-cons-subtype '("a")))))
