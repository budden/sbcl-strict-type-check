
(declaim (optimize (safety 3)))
(proclaim '(optimize (safety 3)))

(defmacro Некое (Тип Выраж)
  "Явное приведение типа, на к-рое SBCL не ругается, поскольку мы его компилируем с соответствующим safety"
  `(let () (declare (optimize (safety 3))) (the ,Тип ,Выраж)))


(declaim (ftype (function (cons) cons) fn-of-cons))
(proclaim '(ftype (function (cons) cons) fn-of-cons)) 

(defun fn-of-cons (x) (Некое cons x))

(eval-when (:compile-toplevel)
  (setf sb-c::|*Запретить-неявное-сужение-типа*| t))

(declaim (optimize (safety 2)))
(proclaim '(optimize (safety 2)))

(declaim (ftype (function ((or rational cons)) cons) alternative-type-fn))
(proclaim '(ftype (function ((or rational cons)) cons) alternative-type-fn)) 

(defun alternative-type-fn (x)
  (fn-of-cons (the cons x)))
