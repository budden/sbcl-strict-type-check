
(defsystem :TYPEP-NEVER-CHANGES-P-TESTS
  :author "Budden73 <budden73@mail.ru>"
  :license "Public Domain"
  :description "Part of typep-never-changes-p project under development (co-developed with https://github.com/budden/sbclt)"
  :depends-on (:typep-never-changes-p)
  :serial t
  :components ((:file "typep-never-changes-p-tests")
               (:file "typep-never-changes-p-tests-2"))
  :perform (test-op (o c)
                    (asdf:load-system :typep-never-changes-p-tests :force t)
                    )
  )

