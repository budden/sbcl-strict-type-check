; -*- system :sbclt-docs; coding: utf-8; -*- 

(named-readtables::in-readtable :buddens-readtable-a)
(in-package :sbclt-docs)

(СЕКЦИЯ GENERAL ("" () "")

  (! интернет "" () "Настроить EMACS")

  (! в-трекер "" () "Сделать для раскладки окон 'пересчитать текущую раскладку окон так, чтобы они ложились поверх текущего положения консоли'

backend-type-predicates может быть интересным. 

Автоматическую раскладку окон сделать на полный размер экрана, возвращаемый оконным менеджером, тогда не нужно будет создавать ненадёжное тестовое окно. 

А также: Изучить недокументированную декларацию '.исбцлт explicit-check'.

А также:
`(defoptimizer (array-element-type derive-type) ((array))` кодирует данные типами, и получается (cons integer), к-рый у нас срезается до cons. Поэтому падает тест coerce derive-type. Вероятно, мы сможем это исправить не раньше, чем добавим иммутабельность.

А также: не работает pgup, pgdn на цифровой клавиатуре в линуксе.

Документировать explicit-check, см. `.исбцл { explicit-check}`

typexpand, typexpand-all - документировать. 

Нужно учесть, что (and immutable integer) == integer - возможно, нужно написать соотв. методы. Также это должно поддерживаться в supertype-such-that-... 

")

  (! трассируем-функции-работы-с-типами "" () """Есть несколько функций, к-рые могут вызываться клиентами, и которые сами могут друг друга вызывать. Хотим узнать, как они вызываются клиентами, но не хотим знать, как они друг друга вызывают. Один из вариантов приблизитьва - трассировать все эти функции, но только при условии, что на стеке нет трассируемой функции. Делается так:

```
(defun trace-depth () (if (boundp 'sb-debug::*traced-entries*)
                       (length sb-debug::*traced-entries*)
                       0))

(trace sb-kernel::csubtypep :condition (= (trace-depth) 0))
; и такая же трассировка для sb-kernel::types-equal-or-intersect, 
; для sb-kernel::type-intersection, sb-kernel::type-intersection2 и, возможно, др. 

sb-c::ir1-phases == 
(trace sb-c::eliminate-dead-code :condition (progn (sb-c::describe-component (sb-debug:arg 0) *standard-output*)))


sb-c::my-ir1-optimize ==

(defun my-ir1-optimize (fn cast) (let ((c cast)) (multiple-value-prog1 (funcall fn cast) (sb-c::describe-object cast *trace-output*))))

(decorate-function:decorate-function 'sb-c::ir1-optimize-cast 'my-ir1-optimize)

SB-C:DEFTRANSFORM ==
(defun my-ir1-transform (fn node transform) ; ir1-transform
             (let ((result (funcall fn node transform)))
              
               (print (if (eq result t) "Transform failed" `("Node after transform: " ,node)) *trace-output*)
               result))

(decorate-function:def-function-decoration ir1-transform 'my-ir1-transform)

```
""")

  (! просмотр-flow-графа-компилятора "" () """ Из старых находок:
Выяснили, что полезно трассировать `(trace sb-c::ir1-phases :break t)`, чтобы получить вид flow графа до начала его обработки, и при этом sb-c::describe-component помогает более-менее внятно посмотреть на его содержимое. 

Для проверки constraint propagation нужно ещё что-то протрассировать и посмотреть. Возможно, надо править describe, чтобы увидет больше сведений о типах. 

В инспекторе происходит ошибка печати, т.к. (я предполагаю) инспектор запускается в отдельном потоке, а нужна переменная `SB-C::*COMPILER-IR-OBJ-MAP*`, связанная только в потоке компилятора. Вроде эту ошибку поправил.
""")

  (! обзор-отладки-SBCL "" () """
Полезны следующие концепции:
- sb-fluid
- sb-show (менее полезна, но я недоразобрался)
- slam.sh 
- горячая пересборка define-type-method возможна при условии горячего переопределения макросов, на к-рых он строится. """ :СМ-ТАКЖЕ (просмотр-flow-графа-компилятора трассируем-функции-работы-с-типами))
  
  (! русские-синонимы-как-боросться-со-string= "" () "Идея для синонимов - печатать имена символов так, чтобы в них был некий запретный character. Тогда при попытке intern синтетического имени возникнет ошибка. Но этот character должен выбрасываться при любой печати, если только не установлена спец. переменная. Ну или вообще чтобы с такими печатанными именами можно было оперировать только в особых условиях, исключающих возможность превращения в символы")

  (! project-goals "" (CONCEPT) """
- Fix the bug with incorrect type inference for (cons integer) by classifying data types as either true types, "typep-never-changes-p" or "type-like predicates". 
- Add immutability support. Some objects are inherently immutable (numbers), some other can be "frozen" at some moment of time and become immutable (freezing idea is from JavaScript, see also freeze-type proclamation in SBCL). Also immutability should be expressed in type system. What does immutable mean depends on base type. 
- Maybe add support for readonly data. Data may be mutable but may not be written by the client of the declaration.
- Add a compiler policy quality, see `SB-C::**POLICY-PRIMARY-QUALITIES**` or a declaration, which disallows implicit upcasts like `(the integer (the number x))`
""")

  (! typep-never-changes-p "" (CONCEPT TYPE-SYSTEM-EXTENSION) """

### Исправление

SB-C::TYPE-NEEDS-CONSERVATION-P - уже похоже на то, что надо, но не используется должным образом. 


typep-never-changes-p для типа означает, что если некий объект в данный момент времени является членом этого типа, то он является членом этого типа на протяжении всей своей жизни. 
Например, таков тип cons. Но типы (cons integer) и (satisfies ufo) не таковы. Причин для нестабильности членства две: 

- зависимость принадлежности к типу от мутабельных свойств объекта
- возможность переопределения самого типа

С мутабельностью всё понятно, а с переопределением труднее:

- cons, number, symbol, array, hash-table никогда не переопределяются 
- мы можем потребовать, чтобы тип никогда не переопределялся - только вместе с unintern его имени. Это может породить другие проблемы, когда тип одноимёнен функции и т.п. 
- то же, более гранулярно - freeze-type 
- декларация 'принадлежность объекта О типу Т не изменится во время использования объекта' (например, внутри let), но переопределение разрешено и ломает код, скомпилированный с этой декларацией
- отслеживание cross-reference information и сообщение о том, что на стеке находится код, являющийся клиентом типа (напр., cerror)
- любой поименованный тип может быть переопределён, например, null, к-рый раскрывается в member. При этом, в коде он обычно развёрнут. Ай-я-яй!

Ф-я sb-c::supertype-such-that-typep-never-changes (смысл следует из названия) для (cons integer) вернёт cons. При передаче информации о типе места в процессе вывода типов, должен быть передан не сам тип, а только возврат этой функции. 


ОГО!  sb-c::lvar-conservative-type похоже на то, что мы хотим!!!
""")

  (! расширение-системы-типов "" (CONCEPT TYPE-SYSTEM-EXTENSION) """
Нам нужны дополнительные атрибуты типов для выражения иммутабельности, неизменности типа во времени (и, возможно, иные). Как этого добиться? 
- добавление нового обёрточного типа `(sb-ext:ty (cons integer) :immutable t)` 
- (cons integer t :immutable t) - уже плохо, что нужно всегда писать типы головы и хвоста, а в других типах может быть ещё хуже
- (and immutable-data (cons integer))
"""
     )

  )


(СЕКЦИЯ DETAILS ("" () "")

  ) ; СЕКЦИЯ

(СЕКЦИЯ SBCL-INFORMATION ("" () "")

  (! как-добавить-новый-вид-определения-в-SBCL "" (SBCL-INFO) """
"c:/yar/lp/budden-tools/sbcl--find-definition-sources-by-name--patch.lisp", 
sb-introspect::sb-introspect--find-definition-sources-by-name-sb--decorated
""")                     

  (! шпаргалка-по-типам-в-SBCL "" (SBCL-INFO) """

### Внутреннее представление типа
```
(sb-kernel::specifier-type 'integer) ; туда
(sb-kernel::type-specifier *) ; и обратно
```

### Проверка пересечения типов
Проверка типов проверяет на непустоту персечения, т.к. у нас вся библиотека нетипизированная!

```
(sb-kernel::types-equal-or-intersect (sb-kernel::specifier-type 'integer) (sb-kernel::specifier-type 'string))
;; и есть такая же для values, может пригодиться для сигнатур методов:
(sb-kernel::values-subtypep (sb-kernel::values-specifier-type '(values integer integer &optional)) (sb-kernel::values-specifier-type '(values rational integer &optional)))

;; допустим, мы создали тип, расширяющийся в существующий defstruct str
(deftype fake-type (argument)
  (declare (ignore argument))
  'str)

;; как узнать, что он есть структура? А вот как:
(sb-kernel::specifier-type '(fake-type 1))
;; ==> #<SB-KERNEL:STRUCTURE-CLASSOID STR>
```
Наследование структур устроено так:
```
тег-самого-старшего-предка
тег-более-молодого-предка
...
тег-этого-типа
```
Хотя возможно, что теги предков содержатся в вынесенном векторе. 
Похоже, что typep на тип структуры работает так: для каждого типа известна глубина его вложенности. 
Сначала тег типа сверяется eq с тегом этой стр-ры. Если нет, то ищется в предках (в известном месте вектора предков, т.к. уровень вложенности предка известен).

`sb-kernel::layout-of` - Return the layout for an object. This is the basic operation for
finding out the "type" of an object, and is used for generic
function dispatch
""")

  (! краткое-описание-системы-типов-SBCL "" (SBCL-INFO) """
### type-class.lisp 

sb-kernel::ctype = compiler type - ссылается на type-class. 

sb-kernel::type-class - разновидность типа, например, named, values, number, hairy. Экземпляры структуры type-class хранятся в `sb-kernel::*type-classes*`. Экземпляр type-class содержит операции над типами такого класса, например, равенство и пересечение типов. 

### Примитивы построения системы типов

sb-kernel::!define-type-class - определяет класс типов
def-type-translator - подобен deftype

sb-kernel::named-type, sb-kernel::member-type - структуры - наследники ctype, ссылаются на экземпляры type-class с именем named, member и т.п. 


""")

  (! define-type-method "" (SBCL-INFO TYPE-INFERENCE) """
 служат для упрощения типов. 
 define-type-method: если не определён, то соотв. операция не меняет тип
 метод для subtype может возврщать два значения, как и сам subtype
 simple методы служат для случая, когда оба операнда имеют одинаковый type-class
 complex методы служат для случая, когда один из аргументов (обычно - первый) имеет иной type-class

### Горячее переопределение
 Похоже, что для возможности горячего переопределения define-type-method нужно две вещи:
1. Переопределить макрос sb-kernel::!cold-init-form
2. Переопределить макрос sb-kernel::!define-type-method

### Пересечение

Методы :simple-intersection :complex-intersection2 описаны возле

sb-kernel::%type-intersection2 


""")

  (! вывод-типов-в-SBCL "" (SBCL-INFO TYPE-INFERENCE) """
Есть, как минимум, 
- SB-C::PROPAGATE-FROM-SETS - система вывода типов
- SB-C::CONSTRAINT-PROPAGATE-IN-BLOCK - независимый от системы типов вычислитель ограничений

Забавно то, что они должны работать вместе, но работают по отдельности, см. примечание у 
SB-C::CONSTRAINT-PROPAGATE-IN-BLOCK, а также FIXME в том же файле. Видимо, на данном этапе мы радуемся тому, что они не взаимодействуют и занимаемся только системой вывода типов.


Проговорим всё от начала до конца. 
```
(defun foo (x y)
  (declare (type foo x))
  (the (cons integer) x) ; теперь мы 'знаем', что x имеет тип 
  (bar)
  (the baz))
```
Это без вывода типов означает:
```
(defun foo (x y)
  (the foo x)
  (the (and foo (cons integer)) x) 
  (bar)
  (the (and foo baz) x))
```
А с выводом типов это означает:
```
(defun foo (x y)
  (the (and foo cons) x)
  (the (and foo (cons integer)) x)
  (bar)
  (the (and foo cons) x))
```
Т.о., очевидно, что у ноды есть два типа - декларированный и выведенный. Декларированный распространяется точно и в обязательном порядке, даже если переменная подвергается присваиванию. 

От выведенного распространяется только conservative-type


""")

   (! структуры-данных-компилятора-SBCL "" (SBCL-INFO) """

### sb-c::component - связный кусок графа потоков
### sb-c::ctran изображает передачу управления в какой-то sb-c::node
### sb-c::node изображает вычисление
### sb-c::valued-node - вычисление, порождающее значение - ссылается на lvar
### sb-c::combination (потомок node) - вызов функции
И отдельный sb-c::mv-combination для mv
### sb-c::сblock (aka block) изображает линейную цепочку из sb-c::node (см. поле start в нём)
```
sb-c::node-derived-type ;  the bottom-up derived type for this node.
```
### Аномалия терминологии
В lvar и ctran есть поля "use(s)", что на самом деле означает 'источник' - откуда lvar получает значение и, соотв., откуда передаётся управление. 

### sb-c::lvar - linear-var временное хранилище данных 

```
;; cached type of this lvar's value. If NIL, then this must be
;; recomputed: see sb-c::LVAR-DERIVED-TYPE
sb-c::lver-%derived-type 

;; Cached type which is checked by DEST. If NIL, then this must be
;; recomputed: see sb-c::LVAR-EXTERNALLY-CHECKABLE-TYPE
sb-c::%externally-checkable-type 
```

### sb-c::leaf - переменная, функция, константа.

```
  ;; the type which values of this leaf must have
  sb-c::leaf-type
  ;; the type which values of this leaf have last been defined to have
  ;; (but maybe won't have in future, in case of redefinition)
  sb-c::leaf-defined-type
```
### sb-c::ref - наследник valued-node, содержит ссылку на leaf

derived-type по умолчанию берётся из leaf-type

Теперь нужно для каждого найти все ссылки на тип и (возможно) будет ясно как расщепить. 

## Устройство памяти (рабочая гипотеза)

В 64 разрядах fixnum - это всё, у чего младший бит равен нулю. 
Поинтер - всё, у чего два младших бита равны единице.
Всё остальное - это 10. 
Остальные два бита поинтера заняты тегом типа поинтера.
Конс - это просто два слова, полностью занятые двумя объектами. В него больше ничего не впихнуть. 

Указатель на конс - два бита, что это поинтер, два бита, что это конс, остальное - адрес. 
Как сделать немутабельным? 
(А) Уменьшить адресуемое пр-во, тогда в указатель (но не в сам конс) можно будет вписать признак мутабельности. 
(Б) Сделать отдельный внешний массив, в к-ром хранятся биты мутабельности:

```
502 CL-USER>(defparameter kb 1024)
KB
503 CL-USER>(defparameter mb (* kb kb))
MB
504 CL-USER>(defparameter gb (* mb kb))
GB
505 CL-USER>(/ gb 16) ; столько байт занимает 1 объект
67108864
506 CL-USER>(/ (dynamic-space-size) 16) ; объект занимает 16 байт, значит у нас всего столько объектов
67108864
509 CL-USER>(defparameter *mutability-table* (make-array 67108864 :element-type '(unsigned-byte 2)))
> (room)
16,777,328 bytes for         3 simple-array-unsigned-byte-2 objects.
```
Т.е. они экономично упакованы. Что тут придётся сделать - так это двигать данные в этом массиве при сборке мусора. Но не будем на это отвлекаться и сделаем пока слабую хеш-таблицу.
А в будущем мы могли бы вынести тег типа в этот массив и тем самым получить бинарную совместимость с Си. 
""")
   
 (! пытаемся-добавить-тип-immutable "" (SBCL-INFO) """
Что нужно учесть: функция freeze на начальном этапе НЕ должна сообщать о том, что аргумент и результат совпадают, поскольку это приведёт к ложному выводу о том, что аргумент был иммутабелен и до момента вызова freeze. Далее неплохо бы сделать так, чтобы инфа о типе распространялась только в будущее, а не в прошлое. 

Что возьмём за основу?
а) !def-type-translator
б) !define-type-class

Уже есть (!define-type-class constant :inherits values). 
Похоже, это про ситуацию, когда значение параметра известно во время компиляции, см., напр, sb-c::constant-function-call-value . Но можно попробовать взять за основу и менять по аналогии. 


""")


 (! сборка-SBCL-быстрее "" (SBCL-INFO) """Этим занимается slam.sh, но он не является build-файлом. Перекомпилирует только изменённые файлы, игнорируя зависимости. Также нужна фича after-xc-core""")

                                 
) ; СЕКЦИЯ
