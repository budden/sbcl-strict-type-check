; -*- system :sbclt-docs; coding: utf-8; -*- 

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :sbclt-docs
  (:always t)
  (:import-from :ОЯ-ИНФРАСТРУКТУРА ОЯ-ИНФРАСТРУКТУРА:! ОЯ-ИНФРАСТРУКТУРА:СЕКЦИЯ)
  (:import-from :named-readtables named-readtables:in-readtable)
  )

