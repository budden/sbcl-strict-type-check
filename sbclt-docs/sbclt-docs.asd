;;; -*- coding: utf-8; Mode: Lisp -*-

;; To recreate, execute the following: (ОЯ-ИНФРАСТРУКТУРА:Собрать-проект-Html-файла :SBCLT-DOCS)

(in-package #:asdf)
(named-readtables::in-readtable :buddens-readtable-a)

(defsystem :SBCLT-DOCS
 :serial t
 :depends-on (:СПРАВОЧНИКИ-HTML)
 :components
  ((:file "package")
   (:file "clear-bd-and-fill-tags")
   (:file "sbclt-docs")
   ))

