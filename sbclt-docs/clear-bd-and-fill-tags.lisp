; -*- system :sbclt-docs ; coding: utf-8; -*- 

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :sbclt-docs)

(cl:setf ОЯ-ИНФРАСТРУКТУРА:†Текущий-проект-Html-файла
      (ОЯ-ИНФРАСТРУКТУРА:MAKE-Проект-файла-Html
       :Имя-пакета :sbclt-docs
       :Теги
       '(
         CONCEPT
         CHANGE
         SBCL-INFO
         INTERFACE
         TYPE-SYSTEM-EXTENSION
         TYPE-INFERENCE
         )
       :Заголовок "SBCLT developer's notes"))

