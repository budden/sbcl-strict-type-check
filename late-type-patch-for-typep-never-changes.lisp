; -*- system: typep-never-changes-p ; -*-

(in-package :SB-KERNEL)

;; it is early-type
#|(defun make-values-type (&key required optional rest allowp)
  (multiple-value-bind (required optional rest)
      (canonicalize-args-type-args required optional rest)
    (cond ((and (null required)
                (null optional)
                (eq rest *universal-type*))
           *wild-type*)
          ((memq *empty-type* required)
           *empty-type*)
          (t
           (let ((typep-never-changes-p
                  (and (every #'type-typep-never-changes-p required)
                       (every #'type-typep-never-changes-p optional)
                       (and rest (type-typep-never-changes-p rest)))))
             
             (make-values-type-cached required optional
                                      rest allowp typep-never-changes-p))))))|#





