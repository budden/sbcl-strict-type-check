; -*- system :TYPEP-NEVER-CHANGES-P ; -*-

(in-package :SB-C)

;;; for a given type, calculate its supertype such that typep never changed for that new type and any object. See also SB-KERNEL::TYPE-TYPEP-NEVER-CHANGES-P in SB-KERNEL::CTYPE structure
;;; FIXME 



;(decorate-function:decorate-function 'sb-c::propagate-from-sets #'my-propagate-from-sets)
; (decorate-function:undecorate-function 'sb-c::propagate-from-sets)

(defvar *do-break* nil)

(defmacro maybe-break (&rest args)
  `(when *do-break*
     (let ((*do-break* nil))
       (break ,@args))))


;; TEMP



(defun my-print-lvar (orig-fn cont)
  (declare (ignore orig-fn))
  (declare (type lvar cont))
  (format t "v<<~D ~S ~S>> " (cont-num cont) (lvar-%derived-type cont) (lvar-%externally-checkable-type cont))
  (values))

(decorate-function:def-function-decoration print-lvar 'my-print-lvar)

(defun my-ir1-phases (orig-fn component)
  ;(describe-component component *standard-output*)
  ;(maybe-break "component")
  (multiple-value-prog1
      (let (;(sb-c::*constraint-propagate* nil)
            )
        (funcall orig-fn component)
        ;(let ((*compiler-trace-output* *trace-output*))
        )))

(decorate-function:def-function-decoration ir1-phases 'my-ir1-phases)

(defun my-ir1-optimize (orig-fn component fastp)
  (multiple-value-prog1
      (funcall orig-fn component fastp)
      (describe-component component *standard-output*)
    (print "------------------------------------------------------------------------------")
    ))

;(decorate-function:def-function-decoration ir1-optimize 'my-ir1-optimize)
;(decorate-function:undecorate-function 'ir1-optimize)

(defun my-print-nodes (orig-fn block)
  (declare (ignore orig-fn))
  (setq block (block-or-lose block))
  (pprint-logical-block (nil nil)
    (format t "~:@_IR1 block ~D start c~D"
            (block-number block) (cont-num (block-start block)))
    (when (block-delete-p block)
      (format t " <deleted>"))

    (pprint-newline :mandatory)
    (awhen (block-info block)
      (format t "start stack: ")
      (print-lvar-stack (ir2-block-start-stack it))
      (pprint-newline :mandatory))
    (do ((ctran (block-start block) (node-next (ctran-next ctran))))
        ((not ctran))
      (let ((node (ctran-next ctran)))
        (format t "~3D>~:[    ~;~:*~3D:~] "
                (cont-num ctran)
                (when (and (valued-node-p node) (node-lvar node))
                  (cont-num (node-lvar node))))
        (when (valued-node-p node)
          (format t "node-derived-type: ~S " (node-derived-type node)))
        (etypecase node
          (ref (print-leaf (ref-leaf node)))
          (basic-combination
           (let ((kind (basic-combination-kind node)))
             (format t "~(~A~A ~A~) "
                     (if (node-tail-p node) "tail " "")
                     kind
                     (type-of node))
             (print-lvar (basic-combination-fun node))
             (dolist (arg (basic-combination-args node))
               (if arg
                   (print-lvar arg)
                   (format t "<none> ")))))
          (cset
           (write-string "set ")
           (print-leaf (set-var node))
           (write-char #\space)
           (print-lvar (set-value node)))
          (cif
           (write-string "if ")
           (print-lvar (if-test node))
           (print-ctran (block-start (if-consequent node)))
           (print-ctran (block-start (if-alternative node))))
          (bind
           (write-string "bind ")
           (print-leaf (bind-lambda node))
           (when (functional-kind (bind-lambda node))
             (format t " ~S ~S" :kind (functional-kind (bind-lambda node)))))
          (creturn
           (write-string "return ")
           (print-lvar (return-result node))
           (print-leaf (return-lambda node)))
          (entry
           (let ((cleanup (entry-cleanup node)))
             (case (cleanup-kind cleanup)
               ((:dynamic-extent)
                (format t "entry DX~{ v~D~}"
                        (mapcar (lambda (lvar-or-cell)
                                  (if (consp lvar-or-cell)
                                      (cons (car lvar-or-cell)
                                            (cont-num (cdr lvar-or-cell)))
                                      (cont-num lvar-or-cell)))
                                (cleanup-info cleanup))))
               (t
                (format t "entry ~S" (entry-exits node))))))
          (exit
           (let ((value (exit-value node)))
             (cond (value
                    (format t "exit ")
                    (print-lvar value))
                   ((exit-entry node)
                    (format t "exit <no value>"))
                   (t
                    (format t "exit <degenerate>")))))
          (cast
           (let ((value (cast-value node)))
             (format t "cast v~D ~A[~S -> ~S]" (cont-num value)
                     (if (cast-%type-check node) #\+ #\-)
                     (cast-type-to-check node)
                     (cast-asserted-type node)))))
        (pprint-newline :mandatory)))

    (awhen (block-info block)
      (format t "end stack: ")
      (print-lvar-stack (ir2-block-end-stack it))
      (pprint-newline :mandatory))
    (let ((succ (block-succ block)))
      (format t "successors~{ c~D~}~%"
              (mapcar (lambda (x) (cont-num (block-start x))) succ))))
  (values))

(decorate-function:def-function-decoration print-nodes 'my-print-nodes)

(defun my-print-leaf (original-fn leaf &optional (stream *standard-output*))
  (declare (type leaf leaf) (type stream stream))
  (typecase leaf
    (lambda-var (format stream "LEAF ~S DEFINED-TYPE ~S" (leaf-debug-name leaf) (leaf-defined-type leaf)))
    (t (funcall original-fn leaf stream))))

(decorate-function:def-function-decoration print-leaf 'my-print-leaf)

;;;; constraint.lisp


