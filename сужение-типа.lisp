;;; -*- coding: utf-8; -*-

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :cl-user)

(declaim (optimize (safety 3)))
(proclaim '(optimize (safety 3)))




(defmacro Некое (Тип Выраж)
  "Явное приведение типа, на к-рое SBCL не ругается, поскольку мы его компилируем с соответствующим safety"
  `(let () (declare (optimize (safety 3))) (the ,Тип ,Выраж)))


(eval-when (:compile-toplevel)
  (setf sb-c::|*Запретить-неявное-сужение-типа*| t))

(declaim (optimize (safety 2)))
(proclaim '(optimize (safety 2)))


(defun PUB2 (X)
  (declare (type NUMBER X))
  (flet ((FOO (Y) 
           (+
            (Некое integer Y)
            1)))
    (FOO X)))


(defun PUB3 (X)
  (declare (type NUMBER X))
  (flet ((FOO (Y) 
           (+
            (the integer Y)
            1)))
    (FOO X)))


#|

 (declaim (ftype (function ((or STRING NUMBER)) t) PUB))
 (proclaim '(ftype (function ((or STRING NUMBER)) t) PUB))

 (defun PUB (X)
  ;(declare (optimize (safety 2)))
  (let ((Y (cons X X)))
    Y)) 

;(defun MGU ()
;  (pub 1.5))

|#