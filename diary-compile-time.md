Информация из компилятора
=======

Постановка задачи
------------------
Есть метаданные: fboundp, MOP, info. Мы хотим видеть образ того, как это видит компилятор. 
Соответственно, наша задача - выяснить, на что можно опираться, а на что нельзя

Анализ
======

Старая информация не уничтожается
---------------------------------
`(sb-impl::show-info)` показывает, что после провалившейся компиляции остаются, как минимум, :function :kind, :function :where-from, :function :type

`(function-lambda-expression 'f1)` не работает и `fbound`, естественно, тоже.

Пытаемся через defun
--------------------
defun -> sb!c:%compiler-defun
  SB-C::*FUN-NAMES-IN-THIS-FILE*
  %set-inline-expansion - пишет прямо в info
  become-defined-fun-name - то же
  proclaim-as-fun-name - то же
    SB-C:*FREE-FUNS* - в компиляторе есть такая фигня - это неопределённые функции.
  
  
SB-C::SUB-COMPILE-FILE - содержит множество переменных, к-рые связываются, например,   
  SB-C::*FUN-NAMES-IN-THIS-FILE* связывается именно здесь
  

deftransform, define-source-transform

ура, deftransform понимает типы. `(sb-c::lvar-derived-type x)`
А как нам теперь понять, что это структурный тип? 