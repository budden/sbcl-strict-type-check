(in-package :cl-user)

(declaim (optimize (safety 2)))
(proclaim '(optimize (safety 2)))

(eval-when (:compile-toplevel)
  (setf sb-c::|*Запретить-неявное-сужение-типа*| t))


(declaim (ftype (function (string) integer) string-length))
(proclaim '(ftype (function (string) integer) string-length))
(declaim (notinline string-length length-of-list))

(defun string-length (x)
  (array-dimension x 0))


(declaim (ftype (function (list) integer) length-of-list))
(proclaim '(ftype (function (list) integer) length-of-list))

(defun length-of-list (x)
  (length x))


(declaim (ftype (function ((or string list)) integer) flexible-length))
(proclaim '(ftype (function ((or string list)) integer) flexible-length))

(defun flexible-length (x)
  (let ((y (the string x)))
    (print y))
  ; (declare (notinline length-of-list string-length))
  ;(declare (ftype (function (string) integer) string-length))
  ;(typecase x
  ;  (string (string-length x))
  ;  (list (length-of-list x)))
  (string-length (the string x))
  )
