; make sure there are no declarations
'#.(dolist (sym '(#:bar #:pub #:buk #:bulk))
     (unintern (intern (string sym))))


(declaim (optimize (safety 2)))
(proclaim '(optimize (safety 2)))

(eval-when (:compile-toplevel)
  (setf sb-c::|*Запретить-неявное-сужение-типа*| t)
  )

(defun BAR (X)
  "Should not warn"
  (declare (type NUMBER X))
  (flet ((FOO (Y) 
           (declare (type NUMBER Y))
           (+ Y 1)))
    (FOO X)))

;; временно отключаем проверку - позволяем сужение без предупреждения
(declaim (optimize (safety 3)))
(defun PUB (X)
  "Should warn"
  (declare (type NUMBER X))
  (flet ((FOO (Y) 
           (declare (type INTEGER Y))
           (+ Y 1)))
    (FOO X)))
  
;(pub 1.5) ; а почему здесь нет предупреждения? Потому что оно работает только внутри ф-й

(declaim (optimize (safety 2)))
(proclaim '(optimize (safety 2)))

(declaim (ftype (function (number) number) buk))
(proclaim '(ftype (function (number) number) buk)) 

(defun buk (x)
  (+ (the number x) 1))

(defun top-level-form ()
  "Здесь есть (должно быть) предупреждение"
  (buk "ыва"))

(declaim (ftype (function (number) number) bulk))
(proclaim '(ftype (function (number) number) bulk)) 

(defun bulk (x)
  "Should warn"
  (+ (the integer x) 1))

