; -*- coding: utf-8; -*-
(in-package :cl-user)

(declaim (optimize (safety 2)))
(proclaim '(optimize (safety 2)))

(eval-when (:compile-toplevel)
  (setf sb-c::|*Запретить-неявное-сужение-типа*| t)
  )

(defstruct SIF1 (f1 nil :type integer))

(defun FNIF1 (b)
  (list
   ;; здесь ошибка показывается у функции, а не у поля - это плохо
   (make-sif1 :f1 b)
   ;; а так это можно вылечить
   (make-sif1 :f1 (the integer b))))

;; при выполнении fn
(fnif1 "а")
