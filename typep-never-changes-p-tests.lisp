; -*- system: typep-never-changes-p-tests ; -*-
(in-package :SB-KERNEL) ;; Sorry for that, that's horrible :) 

(defun tncpos (type)
  "typep-never-changes-p-of-specifier"
  (type-typep-never-changes-p (specifier-type type)))

(defmacro with-test ((&key name) &body body)
  (declare (ignore name))
  `(progn ,@body))

(defun my-predicate (x)
  (declare (ignore x))
  (read-from-string "t"))

(with-test (:name :typep-never-changes-p-must-be)
           (assert (tncpos 'integer))
           (assert (null (tncpos '(satisfies my-predicate))))
           ;; next one is due to sb-kernel::typep-never-changes-for-satisfies-by-predicate-name-p
           (assert (tncpos '(satisfies plusp))) 
           (assert (not (tncpos '(cons integer))))
           (assert (not (tncpos '(cons t integer))))
           (assert (tncpos '(cons t)))
           (assert (tncpos '(cons * t))))

;; supertype-such-that-typep-never-changes-for-specifier
(defun ssttnfs (type)
  (type-specifier (sb-c::supertype-such-that-typep-never-changes (specifier-type type) nil)))

(with-test (:name :are-we-reasoning-about-interned-cons-types-correctly)
           (assert (eq (specifier-type 'cons) (specifier-type '(cons t t))))
           (assert (eq (specifier-type 'cons)
                       (make-cons-type *universal-type* *universal-type*))))


(with-test (:name :supertype-such-that-typep-never-changes-for-specifier)
           (assert (eq 'integer (ssttnfs 'integer)))
           (assert (eq 'cons (ssttnfs 'cons)))
           (assert (eq 'cons (ssttnfs '(cons integer))))
           (assert (eq 'cons (ssttnfs '(cons t integer)))))

(with-test (:name :supertype-such-that-typep-never-changes-negations)
           (assert (eq t (ssttnfs '(not (cons t integer)))))
           (assert (equal '(not array) (ssttnfs '(not (or (cons integer) array))))))

(with-test (:name :supertype-such-that-typep-never-changes-for-values)
           (assert
            (equal '(values cons &optional cons)
                (type-specifier
                 (sb-c::supertype-such-that-typep-never-changes
                  (values-specifier-type '(values (cons integer) &optional (cons array))) nil)))))

